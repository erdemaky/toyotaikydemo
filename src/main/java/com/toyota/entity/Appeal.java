package com.toyota.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "appeal")
public class Appeal {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String departmentName;
	private String tcNo;

	private String name;

	private String personelPhoneCode;
	private String personalPhone;

	private String phoneCode;
	private String phone;

	private String homeCode;
	private String home;

	private String eMail;
	private String nation;

	@Column(length = 11)
	private Date dateOfBirth;

	private boolean gender;

	private boolean criminalRecord;
	private String criminalRecorComment;
	private String criminalReason;

	private int size;
	private int weight;

	private boolean married;

	private String educationLevel;
	private String educationStatus;

	private String universityName;
	private String universityDepartment;
	private String highSchollName;

	private String militaryStatus;
	private String militaryStatusComment;
	private Date militaryDate;
	private String militaryReason;

	private boolean worked;
	private String workedDepartmentName;
	private String reason;

	private double hwIndex;

	private Date createDate;
	private String place;
	private String adress;
	private boolean archive;

	private boolean hearingLoss;

	private String malformed;
	private boolean report;
	private int malformedDegree;

	private boolean signLanguage;

	private Date workStartDate;
	private Date workEndDate;

	private Long basketId;

//	private boolean plan;
//	private boolean ulas;
//	private boolean red;
//
//	public boolean isPlan() {
//		return plan;
//	}
//
//	public void setPlan(boolean plan) {
//		this.plan = plan;
//	}
//
//	public boolean isUlas() {
//		return ulas;
//	}
//
//	public void setUlas(boolean ulas) {
//		this.ulas = ulas;
//	}
//
//	public boolean isRed() {
//		return red;
//	}
//
//	public void setRed(boolean red) {
//		this.red = red;
//	}

	public String getHomeCode() {
		return homeCode;
	}

	public void setHomeCode(String homeCode) {
		this.homeCode = homeCode;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTcNo() {
		return tcNo;
	}

	public void setTcNo(String tcNo) {
		this.tcNo = tcNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPersonelPhoneCode() {
		return personelPhoneCode;
	}

	public void setPersonelPhoneCode(String personelPhoneCode) {
		this.personelPhoneCode = personelPhoneCode;
	}

	public String getPersonalPhone() {
		return personalPhone;
	}

	public void setPersonalPhone(String personalPhone) {
		this.personalPhone = personalPhone;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public boolean isCriminalRecord() {
		return criminalRecord;
	}

	public void setCriminalRecord(boolean criminalRecord) {
		this.criminalRecord = criminalRecord;
	}

	public String getCriminalRecorComment() {
		return criminalRecorComment;
	}

	public void setCriminalRecorComment(String criminalRecorComment) {
		this.criminalRecorComment = criminalRecorComment;
	}

	public String getCriminalReason() {
		return criminalReason;
	}

	public void setCriminalReason(String criminalReason) {
		this.criminalReason = criminalReason;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public boolean isMarried() {
		return married;
	}

	public void setMarried(boolean married) {
		this.married = married;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getEducationStatus() {
		return educationStatus;
	}

	public void setEducationStatus(String educationStatus) {
		this.educationStatus = educationStatus;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public String getUniversityDepartment() {
		return universityDepartment;
	}

	public void setUniversityDepartment(String universityDepartment) {
		this.universityDepartment = universityDepartment;
	}

	public String getHighSchollName() {
		return highSchollName;
	}

	public void setHighSchollName(String highSchollName) {
		this.highSchollName = highSchollName;
	}

	public String getMilitaryStatus() {
		return militaryStatus;
	}

	public void setMilitaryStatus(String militaryStatus) {
		this.militaryStatus = militaryStatus;
	}

	public String getMilitaryStatusComment() {
		return militaryStatusComment;
	}

	public void setMilitaryStatusComment(String militaryStatusComment) {
		this.militaryStatusComment = militaryStatusComment;
	}

	public Date getMilitaryDate() {
		return militaryDate;
	}

	public void setMilitaryDate(Date militaryDate) {
		this.militaryDate = militaryDate;
	}

	public String getMilitaryReason() {
		return militaryReason;
	}

	public void setMilitaryReason(String militaryReason) {
		this.militaryReason = militaryReason;
	}

	public boolean isWorked() {
		return worked;
	}

	public void setWorked(boolean worked) {
		this.worked = worked;
	}

	public String getWorkedDepartmentName() {
		return workedDepartmentName;
	}

	public void setWorkedDepartmentName(String workedDepartmentName) {
		this.workedDepartmentName = workedDepartmentName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public double getHwIndex() {
		return hwIndex;
	}

	public void setHwIndex(double hwIndex) {
		this.hwIndex = hwIndex;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public boolean isHearingLoss() {
		return hearingLoss;
	}

	public void setHearingLoss(boolean hearingLoss) {
		this.hearingLoss = hearingLoss;
	}

	public String getMalformed() {
		return malformed;
	}

	public void setMalformed(String malformed) {
		this.malformed = malformed;
	}

	public boolean isReport() {
		return report;
	}

	public void setReport(boolean report) {
		this.report = report;
	}

	public int getMalformedDegree() {
		return malformedDegree;
	}

	public void setMalformedDegree(int malformedDegree) {
		this.malformedDegree = malformedDegree;
	}

	public boolean isSignLanguage() {
		return signLanguage;
	}

	public void setSignLanguage(boolean signLanguage) {
		this.signLanguage = signLanguage;
	}

	public Date getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(Date workStartDate) {
		this.workStartDate = workStartDate;
	}

	public Date getWorkEndDate() {
		return workEndDate;
	}

	public void setWorkEndDate(Date workEndDate) {
		this.workEndDate = workEndDate;
	}

}
