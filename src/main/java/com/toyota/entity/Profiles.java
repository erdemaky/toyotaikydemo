package com.toyota.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "profiles")
public class Profiles {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String profileName;

	private String genderConstraint;
	private String criminalRecordConstraint;
	private String isMarriedConstraint;
	private String educationLevelConstraint;
	private String isWorkedConstraint;
	private String hearingLossConstraint;
	private String malformedConstraint;

	private int minSizeConstraint;
	private int maxSizeConstraint;

	private int minWeightConstraint;
	private int maxWeightConstraint;

	private int minHwIndexConstraint;
	private int maxHwIndexConstraint;

	public String getMalformedConstraint() {
		return malformedConstraint;
	}

	public void setMalformedConstraint(String malformedConstraint) {
		this.malformedConstraint = malformedConstraint;
	}

	public String getHearingLossConstraint() {
		return hearingLossConstraint;
	}

	public void setHearingLossConstraint(String hearingLossConstraint) {
		this.hearingLossConstraint = hearingLossConstraint;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGenderConstraint() {
		return genderConstraint;
	}

	public void setGenderConstraint(String genderConstraint) {
		this.genderConstraint = genderConstraint;
	}

	public String getCriminalRecordConstraint() {
		return criminalRecordConstraint;
	}

	public void setCriminalRecordConstraint(String criminalRecordConstraint) {
		this.criminalRecordConstraint = criminalRecordConstraint;
	}

	public String getIsMarriedConstraint() {
		return isMarriedConstraint;
	}

	public void setIsMarriedConstraint(String isMarriedConstraint) {
		this.isMarriedConstraint = isMarriedConstraint;
	}

	public String getIsWorkedConstraint() {
		return isWorkedConstraint;
	}

	public void setIsWorkedConstraint(String isWorkedConstraint) {
		this.isWorkedConstraint = isWorkedConstraint;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getEducationLevelConstraint() {
		return educationLevelConstraint;
	}

	public void setEducationLevelConstraint(String educationLevelConstraint) {
		this.educationLevelConstraint = educationLevelConstraint;
	}

	public int getMinSizeConstraint() {
		return minSizeConstraint;
	}

	public void setMinSizeConstraint(int minSizeConstraint) {
		this.minSizeConstraint = minSizeConstraint;
	}

	public int getMaxSizeConstraint() {
		return maxSizeConstraint;
	}

	public void setMaxSizeConstraint(int maxSizeConstraint) {
		this.maxSizeConstraint = maxSizeConstraint;
	}

	public int getMinWeightConstraint() {
		return minWeightConstraint;
	}

	public void setMinWeightConstraint(int minWeightConstraint) {
		this.minWeightConstraint = minWeightConstraint;
	}

	public int getMaxWeightConstraint() {
		return maxWeightConstraint;
	}

	public void setMaxWeightConstraint(int maxWeightConstraint) {
		this.maxWeightConstraint = maxWeightConstraint;
	}

	public int getMinHwIndexConstraint() {
		return minHwIndexConstraint;
	}

	public void setMinHwIndexConstraint(int minHwIndexConstraint) {
		this.minHwIndexConstraint = minHwIndexConstraint;
	}

	public int getMaxHwIndexConstraint() {
		return maxHwIndexConstraint;
	}

	public void setMaxHwIndexConstraint(int maxHwIndexConstraint) {
		this.maxHwIndexConstraint = maxHwIndexConstraint;
	};
}
