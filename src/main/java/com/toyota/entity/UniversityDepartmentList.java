package com.toyota.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UniversityDepartmentList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String universityDeparmentName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUniversityDeparmentName() {
		return universityDeparmentName;
	}

	public void setUniversityDeparmentName(String universityDeparmentName) {
		this.universityDeparmentName = universityDeparmentName;
	}

}
