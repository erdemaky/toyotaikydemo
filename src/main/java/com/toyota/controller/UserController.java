package com.toyota.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.toyota.entity.Profiles;
import com.toyota.entity.User;
import com.toyota.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "admin/user/delete", method = RequestMethod.POST)
	public String deleteUser(HttpServletRequest request,Model model,@RequestParam("id") Long id) {
		userService.deleteUser(id);
	    String referer = request.getHeader("Referer");
	    
	    return "redirect:"+ referer;
	}
	
	@RequestMapping("admin/user")
	public String getUsers(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("users", userService.getAllUsers());
		
		return "admin/user";
	}
	
	@RequestMapping(value = "admin/user/new", method = RequestMethod.POST)
	public String saveProfile(Model model,User user) throws NoSuchAlgorithmException{		
		userService.addUser(user);
		model.addAttribute("user", new User());
		model.addAttribute("users", userService.getAllUsers());
		
		return "admin/user";
	}


}
