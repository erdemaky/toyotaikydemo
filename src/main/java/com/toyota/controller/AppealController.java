package com.toyota.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.toyota.entity.Appeal;
import com.toyota.entity.UniversityDepartmentList;
import com.toyota.entity.UniversityList;
import com.toyota.repository.UniversityDepartmentRepository;
import com.toyota.repository.UniversityRepository;
import com.toyota.service.AppealService;
import com.toyota.service.BasketService;

@Controller
public class AppealController {

	@Autowired
	AppealService appealService;
	
	@Autowired
	UniversityRepository universityRepository;
	
	@Autowired
	UniversityDepartmentRepository universityDepartmentRepository;

	@PersistenceContext
	protected EntityManager em;

	@RequestMapping("/")
	public String edit(Model model) {
		Appeal appeal = new Appeal();
		model.addAttribute("appeal", appeal);
		
		Iterable<UniversityList> universityLists = universityRepository.findAll();
		Iterable<UniversityDepartmentList> universityDepartmentLists = universityDepartmentRepository.findAll();
		
		model.addAttribute("universityList", universityLists);
		model.addAttribute("departmentList", universityDepartmentLists);
		
		return "ToyotaAppealForm";
	}

	@RequestMapping("appeal/new")
	public String newAppeal(Model model) {
		model.addAttribute("appeal", new Appeal());
		return "appealform";
	}

	@RequestMapping(value = "appeal", method = RequestMethod.POST)
	public String saveAppeal(Appeal appeal) {
		double hwIndex = 0.0;
		try {
			double size = ((double) appeal.getSize()) / 100;
			size = size * size;

			hwIndex = appeal.getWeight() / size;

			hwIndex = BigDecimal.valueOf(hwIndex).setScale(1, RoundingMode.HALF_UP).doubleValue();
		} catch (Exception e) {
			// TODO: handle exception
		}

		appeal.setHwIndex(hwIndex);

		appeal.setCreateDate(new Date());
		appeal.setPersonalPhone(appeal.getPersonelPhoneCode() + "" + appeal.getPersonalPhone());
		appeal.setPhone(appeal.getPhoneCode() + "" + appeal.getPhone());
		appeal.setHome(appeal.getHomeCode() + "" + appeal.getHome());
		
		appealService.addAppeal(appeal);
		return "Success";
	}

	@RequestMapping(value = "/appeals/find", method = RequestMethod.POST)
	public String findAppeals(Model model, String name) {

		model.addAttribute("appeals", appealService.findByName(name));
		return "appeals";
	}

}
