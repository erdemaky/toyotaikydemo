package com.toyota.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.toyota.entity.Profiles;
import com.toyota.service.ProfilesService;

@Controller
public class ProfileController {

	@Autowired
	private ProfilesService profilesService;
	
	@RequestMapping("admin/profiles")
	public String newProfil(Model model) {
		model.addAttribute("profiles", new Profiles());		
		model.addAttribute("allProfiles", profilesService.getAllProfiles());
		
		return "admin/profiles";
	}
	
	@RequestMapping(value = "admin/profile/new", method = RequestMethod.POST)
	public String saveProfile(Model model,Profiles profiles){
		profilesService.addProfile(profiles);
		model.addAttribute("profiles", new Profiles());
		model.addAttribute("allProfiles", profilesService.getAllProfiles());
		
		return "admin/profiles";
	}
	
	
	@RequestMapping(value = "admin/profile/delete", method = RequestMethod.POST)
	public String deleteProfil(Model model,@RequestParam("id") Long id){
		profilesService.deleteProfile(id);
		
		model.addAttribute("profiles", new Profiles());		
		model.addAttribute("allProfiles", profilesService.getAllProfiles());
		
		return "admin/profiles";
	}
	
	
}
