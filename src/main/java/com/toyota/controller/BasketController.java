package com.toyota.controller;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.toyota.entity.Appeal;
import com.toyota.entity.Basket;
import com.toyota.service.AppealService;
import com.toyota.service.BasketService;

@Controller
public class BasketController {

	@Autowired
	private BasketService basketService;
	
	@Autowired
	private AppealService appealService;
	
	@RequestMapping(value = "admin/basket/edit", method=RequestMethod.POST, params="action=Sil")
	public String deleteList(HttpServletRequest request,Model model,@RequestParam("id") Long id) {
		basketService.deleteBasket(id);
	    String referer = request.getHeader("Referer");
	    
	    return "redirect:"+ referer;
	}
	
	@RequestMapping(value = "admin/basket/edit", method=RequestMethod.POST, params="action=Görüntüle")
	public ModelAndView showList(HttpServletRequest request,Model model,@RequestParam("id") Long id) {
		model.addAttribute("listId", id);
		return new ModelAndView("redirect:/admin/appeals/?id="+id);
	}
	
	@RequestMapping("admin/basket")
	public String getBaskets(Model model) {
		model.addAttribute("basket", new Basket());
		model.addAttribute("baskets", basketService.findAllBasket());
		
		return "admin/lists";
	}
	
	@RequestMapping(value = "admin/basket/new", method = RequestMethod.POST)
	public String saveProfile(Model model,Basket basket) throws NoSuchAlgorithmException{		
		basketService.addBasket(basket);
		model.addAttribute("basket", new Basket());
		model.addAttribute("baskets", basketService.findAllBasket());
		
		return "admin/lists";
	}
	
	@RequestMapping(value = "admin/basket/addappeal", method = RequestMethod.POST)
	public ResponseEntity saveProfile(@RequestParam("id") Long basketId, @RequestParam("appealId") Long appealId) throws NoSuchAlgorithmException{		
		
		Appeal appel = appealService.getAppealById(appealId);
		appel.setBasketId(basketId);
		appealService.updateAppeal(appel);
		
		return new ResponseEntity(HttpStatus.OK);
	}
	
}
