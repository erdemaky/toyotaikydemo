package com.toyota.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.toyota.entity.Appeal;
import com.toyota.entity.Profiles;
import com.toyota.service.AppealService;
import com.toyota.service.BasketService;
import com.toyota.service.ProfilesService;

@Controller
public class AdminController {

	@Autowired
	private BasketService basketService;

	@Autowired
	AppealService appealService;

	@Autowired
	private ProfilesService profilesService;
	
	@PersistenceContext
	protected EntityManager em;
	
	@RequestMapping("/admin")
	public String adminPage() {
		return "admin/login";
	}

	@RequestMapping("/admin/appeal/delete/{id}")
	public String delete(HttpServletRequest request,Model model,@PathVariable Long id) {
		appealService.deleteAppeal(id);
	    String referer = request.getHeader("Referer");
	    
	    return "redirect:"+ referer;
	}
	
	@RequestMapping(value = "admin/appeals", method = RequestMethod.GET)
	public String getAppeals(Model model, @RequestParam(value = "profileId", defaultValue = "0") Long profilId,
			@RequestParam(value = "id", defaultValue = "0") Long listId) {

		Iterable<Appeal> appeals = null;
		
		model.addAttribute("show",true);
		if (listId != null && listId != 0){
			model.addAttribute("show",false);
			
			appeals = appealService.findByBasketId(listId);
		}else if (profilId != null && profilId != 0) {
			Profiles profile = profilesService.getProfile(profilId);
			Session session = (Session) em.getDelegate();

			Criteria criteria = session.createCriteria(Appeal.class);
			
			criteria.add(Restrictions.eq("archive", false));

			String mString = profile.getMalformedConstraint();
			if (!mString.equals("*")) {
				boolean mB = Boolean.parseBoolean(mString);
				if (mB == true){
					criteria.add(Restrictions.lt("malformedDegree", 40));
				}else{
					criteria.add(Restrictions.gt("malformedDegree", 40));
				}
				
			}
			
			String sHear = profile.getHearingLossConstraint();
			if (!sHear.equals("*")) {
//				boolean hear = Boolean.parseBoolean(sHear);
				criteria.add(Restrictions.eq("malformed", sHear));
			}
			
			String sGender = profile.getGenderConstraint();
			if (!sGender.equals("*")) {
				boolean gender = Boolean.parseBoolean(sGender);
				criteria.add(Restrictions.eq("gender", gender));
			}

			String criminalRecord = profile.getCriminalRecordConstraint();
			if (!criminalRecord.equals("*")) {
				boolean criminal = Boolean.parseBoolean(criminalRecord);
				criteria.add(Restrictions.eq("criminalRecord", criminal));
			}

			String isMarried = profile.getIsMarriedConstraint();
			if (!isMarried.equals("*")) {
				boolean married = Boolean.parseBoolean(isMarried);
				criteria.add(Restrictions.eq("married", married));
			}

			String isWorked = profile.getIsWorkedConstraint();
			if (!isWorked.equals("*")) {
				boolean worked = Boolean.parseBoolean(isWorked);
				criteria.add(Restrictions.eq("worked", worked));
			}

			String educationLevel = profile.getEducationLevelConstraint();

			if (educationLevel != null && !educationLevel.equals("*")) {
				String[] eduArray = educationLevel.split(",");

				List<Criterion> criterions = new ArrayList<>();
				if (eduArray != null) {
					for (String string : eduArray) {
						criterions.add(Restrictions.eq("educationLevel", string));
					}
				}

				if (!criterions.isEmpty()) {
					criteria.add(or(criterions));
				}
			}

			if (profile.getMinSizeConstraint() != 0 || profile.getMaxSizeConstraint() != 0)
				criteria.add(
						Restrictions.between("size", profile.getMinSizeConstraint(), profile.getMaxSizeConstraint()));

			if (profile.getMinWeightConstraint() != 0 || profile.getMaxWeightConstraint() != 0)
				criteria.add(Restrictions.between("weight", profile.getMinWeightConstraint(),
						profile.getMaxWeightConstraint()));

			if (profile.getMinHwIndexConstraint() != 0 || profile.getMaxHwIndexConstraint() != 0)
				criteria.add(Restrictions.between("hwIndex", (double) profile.getMinHwIndexConstraint(),
						(double) profile.getMaxHwIndexConstraint()));

			appeals = criteria.list();
		} else {
			appeals = appealService.findByArchive(false);
		}

		model.addAttribute("appeals", appeals);
		model.addAttribute("allProfiles", profilesService.getAllProfiles());
		model.addAttribute("selectedProfileId", profilId);
		model.addAttribute("baskets", basketService.findAllBasket());

		return "admin/appeals";
	}

	@RequestMapping("admin/appeal/{id}")
	public String showAppeal(@PathVariable Long id, Model model) {
		model.addAttribute("appeal", appealService.getAppealById(id));
		return "admin/appeal";
	}
	
	private Disjunction or(List<Criterion> restrictions) {
		Disjunction result = Restrictions.disjunction();

		for (Criterion restriction : restrictions) {
			result.add(restriction);
		}

		return result;
	}
	
}
