package com.toyota.repository;

import org.springframework.data.repository.CrudRepository;

import com.toyota.entity.UniversityDepartmentList;

public interface UniversityDepartmentRepository extends CrudRepository<UniversityDepartmentList, Long>{

}
