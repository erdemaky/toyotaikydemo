package com.toyota.repository;

import org.springframework.data.repository.CrudRepository;

import com.toyota.entity.UniversityList;

public interface UniversityRepository extends CrudRepository<UniversityList, Long>{

}
