package com.toyota.repository;

import org.springframework.data.repository.CrudRepository;

import com.toyota.entity.Profiles;

public interface ProfilesRepository  extends CrudRepository<Profiles, Long>{

	Profiles getProfilesById(Long id);
	
}
