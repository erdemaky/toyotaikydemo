package com.toyota.repository;

import org.springframework.data.repository.CrudRepository;

import com.toyota.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findByName(String name);
	
}
