package com.toyota.repository;

import org.springframework.data.repository.CrudRepository;

import com.toyota.entity.Basket;

public interface BasketRepository extends CrudRepository<Basket, Long>{

	Iterable<Basket> findByName(String name);
}
