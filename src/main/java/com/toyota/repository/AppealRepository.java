package com.toyota.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.toyota.entity.Appeal;

public interface AppealRepository extends CrudRepository<Appeal, Long>{

	Appeal getAppealById(Long id);

	Set<Appeal> findByNameContaining(String name);
	
	Iterable<Appeal> findByArchive(boolean archive);
	
	Iterable<Appeal> findByBasketId(Long basketId);
}
