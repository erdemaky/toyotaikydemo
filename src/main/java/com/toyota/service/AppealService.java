package com.toyota.service;

import java.util.Set;

import com.toyota.entity.Appeal;

public interface AppealService {
	
	void addAppeal(Appeal appeal);
	
	Appeal getAppealById(Long id);
	
	Iterable<Appeal> getAllAppeal();
	
	void deleteAppeal(Long id);
	
	Set<Appeal> findByName(String name);
	
	Iterable<Appeal> findByArchive(boolean archive);
	
	void updateAppeal(Appeal appeal);
	
	Iterable<Appeal> findByBasketId(Long basketId);
}
