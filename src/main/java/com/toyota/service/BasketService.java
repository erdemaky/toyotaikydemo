package com.toyota.service;

import com.toyota.entity.Basket;

public interface BasketService {

	Iterable<Basket> findAllBasket();
	
	Iterable<Basket> findByName(String name);

	void addBasket(Basket basket);

	void deleteBasket(Long id);

}
