package com.toyota.service;

public interface AuthService {
	
	boolean authUser(String username, String credential);
	
}
