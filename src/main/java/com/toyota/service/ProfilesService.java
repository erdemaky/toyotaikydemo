package com.toyota.service;


import com.toyota.entity.Profiles;

public interface ProfilesService {

	void addProfile(Profiles profiles);
	
	void editProfile(Profiles profile);
	
	void deleteProfile(Long id);
	
	Iterable<Profiles> getAllProfiles();
	
	Profiles getProfile(Long id);
	
}
