package com.toyota.service;

import java.security.NoSuchAlgorithmException;

import com.toyota.entity.User;

public interface UserService {

	User findByName(String name);
	
	void deleteUser(Long id);
	
	Iterable<User> getAllUsers();
	
	void addUser(User user) throws NoSuchAlgorithmException;
	
}
