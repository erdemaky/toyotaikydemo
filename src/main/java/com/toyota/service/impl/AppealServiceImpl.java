package com.toyota.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toyota.entity.Appeal;
import com.toyota.repository.AppealRepository;
import com.toyota.service.AppealService;

@Service
public class AppealServiceImpl implements AppealService {

	@Autowired
	private AppealRepository appealRepository;

	@Override
	public void addAppeal(Appeal appeal) {
		appealRepository.save(appeal);
	}

	@Override
	public Iterable<Appeal> getAllAppeal() {
		return appealRepository.findAll();
	}

	@Override
	public Appeal getAppealById(Long id) {
		return appealRepository.getAppealById(id);
	}

	@Override
	public void deleteAppeal(Long id) {
		Appeal appeal = appealRepository.getAppealById(id);
		appeal.setArchive(true);
		
		appealRepository.save(appeal);
	}

	@Override
	public Set<Appeal> findByName(String name) {
		return appealRepository.findByNameContaining(name);
	}

	@Override
	public Iterable<Appeal> findByArchive(boolean archive) {
		return appealRepository.findByArchive(archive);
	}
	
	@Override
	public void updateAppeal(Appeal appeal) {
		appealRepository.save(appeal);
	}

	@Override
	public Iterable<Appeal> findByBasketId(Long basketId) {
		return appealRepository.findByBasketId(basketId);
	}
	
	
	
}
