package com.toyota.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toyota.entity.User;
import com.toyota.repository.UserRepository;
import com.toyota.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User findByName(String name) {
		return userRepository.findByName(name);
	}

	@Override
	public void deleteUser(Long id) {
		userRepository.delete(id);
	}

	@Override
	public Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public void addUser(User user) throws NoSuchAlgorithmException {
		if (userRepository.findByName(user.getName()) == null) {

			MessageDigest md5Digest = MessageDigest.getInstance("MD5");
			md5Digest.update(user.getPassword().getBytes(), 0, user.getPassword().length());
			String md5Password = new BigInteger(1, md5Digest.digest()).toString(16);
			user.setPassword(md5Password);

			userRepository.save(user);
		}
	}

}
