package com.toyota.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toyota.entity.Profiles;
import com.toyota.repository.ProfilesRepository;
import com.toyota.service.ProfilesService;

@Service
public class ProfilesServiceImpl implements ProfilesService{

	@Autowired
	private ProfilesRepository profilesRepository;

	@Override
	public void addProfile(Profiles profiles) {
		profilesRepository.save(profiles);
	}

	@Override
	public void editProfile(Profiles profile) {
		profilesRepository.save(profile);
	}

	@Override
	public void deleteProfile(Long id) {
		profilesRepository.delete(id);
	}

	@Override
	public Iterable<Profiles> getAllProfiles() {
		return profilesRepository.findAll();
	}

	@Override
	public Profiles getProfile(Long id) {
		return profilesRepository.getProfilesById(id);
	}
	
	
	
}
