package com.toyota.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toyota.entity.User;
import com.toyota.repository.UserRepository;
import com.toyota.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean authUser(String username, String credential) {
		User dbUser = userRepository.findByName(username);

		if (dbUser == null)
			return false;
			
		MessageDigest md5Digest;
		try {
			md5Digest = MessageDigest.getInstance("MD5");
			md5Digest.update(credential.getBytes(), 0, credential.length());
			String md5Password = new BigInteger(1, md5Digest.digest()).toString(16);
			if (dbUser.getPassword().equals(md5Password)) {
				return true;
			}
		} catch (NoSuchAlgorithmException e) {
			if (dbUser.getPassword().equals(credential)) {
				return true;
			}
		}
		return false;
	}

}
