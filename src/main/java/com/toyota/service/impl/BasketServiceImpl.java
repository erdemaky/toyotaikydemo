package com.toyota.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toyota.entity.Basket;
import com.toyota.repository.BasketRepository;
import com.toyota.service.BasketService;

@Service
public class BasketServiceImpl implements BasketService {

	@Autowired
	private BasketRepository basketRepository;
	
	@Override
	public Iterable<Basket> findByName(String name) {
		return basketRepository.findByName(name);
	}

	@Override
	public void addBasket(Basket basket) {
		basketRepository.save(basket);
	}

	@Override
	public void deleteBasket(Long id) {
		basketRepository.delete(id);
	}

	@Override
	public Iterable<Basket> findAllBasket() {
		return basketRepository.findAll();
	}

}
