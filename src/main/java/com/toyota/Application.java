package com.toyota;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.toyota.entity.User;
import com.toyota.service.UserService;

@SpringBootApplication
public class Application {

	@Autowired
	private UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			
			//Test için bir kullanıcı ekleniyor
			User user = new User();
			user.setName("demo");
			user.setPassword("demo");
			
			userService.addUser(user);
		};
	}

}